package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	import starling.core.Starling;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	[SWF(width="960", height="540", frameRate="40", backgroundColor="0x222222")]
	public class StarlingSkeleton extends Sprite
	{
		
		private var mStarling:Starling;
		
		public function StarlingSkeleton()
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		protected function init(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			mStarling = new Starling(Main, stage);
			mStarling.antiAliasing = 1;
			mStarling.showStatsAt(HAlign.LEFT, VAlign.BOTTOM);
			mStarling.start();
			
		}
	}
}